# Moving to GitLab

https://gitlab.com/johannes303


## Alpine-Java8 Base Docker Container #

Minimalistic docker base container based on Alpine Linux. It could further be shrinked by deleting unneeded stuff like i.e JavaFX. It has some infrastructure around it to run tests against the container (optionally). These are written in Ruby using RSpec.

The idea is based on a talk given by Moritz Heiber (https://heiber.im/).


### How to install ###

* First install Ruby
* then add bundler to it
`$ gem install bundler`
`$ bundle install --path vendor/bundle`
* run the tests
`$ bundle exec rspec spec/alpine-java/alpine-java_spec.rb`
