require 'spec_helper'

image = Docker::Image.build_from_dir(
  '.',
  'dockerfile' => 'Dockerfile'
)

describe 'alpine-java' do
  set :docker_image, image.id

  describe 'package curl' do
    it 'should be installed' do
      expect(package('curl')).to be_installed
    end
  end

  describe 'package ca-certificates' do
    it 'should be installed' do
      expect(package('ca-certificates')).to be_installed
    end
  end

  describe 'package bash' do
    it 'should be installed' do
      expect(package('bash')).to be_installed
    end
  end

  describe 'package glibc' do
    it 'should be installed' do
      expect(package('glibc')).to be_installed
    end
  end

 describe 'package libgcc' do
    it 'should be installed' do
      expect(package('libgcc')).to be_installed
    end
  end

 describe 'package glibc-bin' do
    it 'should be installed' do
      expect(package('glibc-bin')).to be_installed
    end
  end

  describe 'package glibc-i18n' do
    it 'should be installed' do
      expect(package('glibc-i18n')).to be_installed
    end
  end
end
